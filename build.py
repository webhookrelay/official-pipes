import glob
import json
import logging
import re
import subprocess
import sys
from concurrent.futures.thread import ThreadPoolExecutor
from dataclasses import dataclass, asdict
from datetime import datetime
from typing import List, Optional
import warnings

import cerberus
import docker
import requests
import yaml
from colorlog.colorlog import ColoredFormatter
from requests.exceptions import HTTPError


def logger_config():
    """
        Setup the logging environment
    """

    clogger = logging.getLogger()
    format_str = '%(log_color)s%(levelname)s: %(message)s'
    date_format = '%Y-%m-%d %H:%M:%S'
    colors = {'DEBUG': 'green',
              'INFO': 'blue',
              'WARNING': 'bold_yellow',
              'ERROR': 'bold_red',
              'CRITICAL': 'bold_purple'}
    formatter = ColoredFormatter(format_str, date_format, log_colors=colors)
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(formatter)
    clogger.addHandler(stream_handler)
    clogger.setLevel(logging.INFO)
    return clogger


logger = logger_config()

WHITELIST_PIPES = {
    'pipes/anchore-scan.yml': ['0.2.3 version available. Current version is 0.1.2'],
    'pipes/azure-aks-deploy.yml': ['pipes/azure-aks-deploy.yml yml field not equal'],
    'pipes/azure-aks-helm-deploy.yml': ['pipes/azure-aks-helm-deploy.yml yml field not equal'],
    'pipes/azure-arm-deploy.yml': ['pipes/azure-arm-deploy.yml yml field not equal'],
    'pipes/azure-cli-run.yml': ['pipes/azure-cli-run.yml yml field not equal'],
    'pipes/azure-functions-deploy.yml': ['pipes/azure-functions-deploy.yml yml field not equal'],
    'pipes/azure-vm-linux-script-deploy.yml': ['pipes/azure-vm-linux-script-deploy.yml yml field not equal'],
    'pipes/azure-vmss-linux-script-deploy.yml': ['pipes/azure-vmss-linux-script-deploy.yml yml field not equal'],
    'pipes/azure-web-apps-containers-deploy.yml': ['pipes/azure-web-apps-containers-deploy.yml yml field not equal'],
    'pipes/azure-web-apps-deploy.yml': ['pipes/azure-web-apps-deploy.yml yml field not equal'],
    'pipes/cerberus-testing-runner.yml': ["pipes/cerberus-testing-runner.yml repository not valid: pipe.yml not valid: {'repository': ['required field']}", 'pipes/cerberus-testing-runner.yml yml definition in README.md is not valid'], 'errors': [],
    'pipes/debricked-scan.yml': ['1.0.2 version available. Current version is 1.0.1'],
    'pipes/ld-find-code-refs-pipe.yml': ['1.5.1 version available. Current version is 1.0.0', 'pipes/ld-find-code-refs-pipe.yml yml field not equal'],
    'pipes/rollbar-notify.yml': ['pipes/rollbar-notify.yml yml field not equal'],
    'pipes/rolloutio-flag-templates.yml': ['pipes/rolloutio-flag-templates.yml yml field not equal'],
    'pipes/snyk-scan.yml': ['pipes/snyk-scan.yml yml field not equal'],
    'pipes/sonatype-nexus-repository-publish.yml': ['pipes/sonatype-nexus-repository-publish.yml yml field not equal'],
    'pipes/whitesource-scan.yml': ['1.3.0 version available. Current version is 1.1.0', 'pipes/whitesource-scan.yml yml field not equal', "pipes/whitesource-scan.yml repository not valid: pipe.yml not valid: {'image': ['Docker images larger than 1GB not supported']}"],
    'pipes/google-app-engine-deploy.yml': ["pipes/google-app-engine-deploy.yml repository not valid: pipe.yml not valid: {'image': ['Docker images larger than 1GB not supported']}"],
    'pipes/google-gke-kubectl-run.yml': ["pipes/google-gke-kubectl-run.yml repository not valid: pipe.yml not valid: {'image': ['Docker images larger than 1GB not supported']}"],
    'pipes/synopsys-detect.yml': ["pipes/synopsys-detect.yml repository not valid: pipe.yml not valid: {'image': ['Docker images larger than 1GB not supported']}"],
    'pipes/meterian-scan.yml': ["pipes/meterian-scan.yml repository not valid: pipe.yml not valid: {'image': ['Docker images larger than 1GB not supported']}"],
}


ALLOWED_CATEGORIES = ['Alerting', 'Artifact management', 'Code quality', 'Deployment', 'Feature flagging',
                      'Monitoring', 'Notifications', 'Security', 'Testing', 'Utilities', 'Workflow automation']

REMOTE_METADATA = cerberus.Validator({
    'name': {'type': 'string', 'required': True},
    'image': {'type': 'string', 'required': True},
    'description': {'type': 'string', 'required': True},
    'category': {'type': 'string', 'required': False, 'allowed': ALLOWED_CATEGORIES},
    'repository': {'type': 'string', 'required': True},
    'maintainer': {
        'type': 'dict',
        'schema': {
            'name': {'type': 'string', 'required': True},
            'website': {'type': 'string', 'required': True},
            'email': {'type': 'string', 'required': False}
        },
        'required': False
    },
    'tags': {'type': 'list', 'required': True},

    'vendor': {
        'type': 'dict',
        'schema': {
            'name': {'type': 'string', 'required': True},
            'website': {'type': 'string', 'required': True},
            'email': {'type': 'string', 'required': False}
        },
        'required': False
    },
    'variables': {'type': 'list', 'required': False},
    # TODO: For backward compatibility. Remove `icon` field once custom pipes will migrate
    'icon': {'type': 'string', 'required': False},
})

LOCAL_METADATA = cerberus.Validator({
    'repositoryPath': {'type': 'string', 'required': True},
    'version': {'type': 'string', 'required': True},

    # for backward compatibility with custom pipes. TODO: migrate to proposed structure once custom pipes will migrate
    'name': {'type': 'string', 'required': False},
    'description': {'type': 'string', 'required': False},
    'category': {'type': 'string', 'required': False, 'allowed': ALLOWED_CATEGORIES},
    'logo': {'type': 'string', 'required': False},
    'vendor': {
        'type': 'dict',
        'schema': {
            'name': {'type': 'string', 'required': True},
            'website': {'type': 'string', 'required': True},
            'email': {'type': 'string', 'required': False}
        },
        'required': False
    },
    'maintainer': {
        'type': 'dict',
        'schema': {
            'name': {'type': 'string', 'required': True},
            'website': {'type': 'string', 'required': True},
            'email': {'type': 'string', 'required': False}
        },
        'required': False
    },
    'yml': {'type': 'string', 'required': False},
    'tags': {'type': 'list', 'required': False},
})

FINAL_METADATA = cerberus.Validator({
    'name': {'type': 'string', 'required': True},
    'description': {'type': 'string', 'required': True},
    'category': {'type': 'string', 'required': True, 'allowed': ALLOWED_CATEGORIES},
    'logo': {'type': 'string', 'required': True},
    'repositoryPath': {'type': 'string', 'required': True},
    'version': {'type': 'string', 'required': True},
    'maintainer': {
        'type': 'dict',
        'schema': {
            'name': {'type': 'string', 'required': True},
            'website': {'type': 'string', 'required': True},
            'email': {'type': 'string', 'required': False}
        },
        'required': True
    },
    'vendor': {
        'type': 'dict',
        'schema': {
            'name': {'type': 'string', 'required': True},
            'website': {'type': 'string', 'required': True},
            'email': {'type': 'string', 'required': False}
        },
        'required': False
    },
    'yml': {'type': 'string', 'required': True},
    'tags': {'type': 'list', 'required': True},
    'timestamp': {'type': 'string', 'required': True}
})


@dataclass
class ProcessMetadata:
    file: str
    valid: bool
    failures: List[str]
    errors: List[str]
    skipped: List[str]


@dataclass
class ProcessResult:
    metadata: ProcessMetadata
    data: Optional[dict]


@dataclass
class RuleContext:
    infile: str
    local_yml: dict
    remote_yml: Optional[dict]
    final_yml: dict


class DockerHubAPIService:

    def get_size(self, docker_image_path: str) -> int:
        array = docker_image_path.split(':')
        tag = "latest"
        docker_image = array[0]
        if len(array) == 2:
            tag = array[1]
        request = requests.get(f"https://hub.docker.com/v2/repositories/{docker_image}/tags/{tag}")
        request.raise_for_status()
        request.close()
        size = request.json()['full_size']
        return int(size)


class BitbucketCloudService:

    def get_latest_tag(self, repository_path: str) -> Optional[str]:
        """
        Gets the latest tag of a repository (sort alphabetically)
        :param repository_path: Repository path (ex: account/repo)
        :return: Latest tag
        """
        request = requests.get(f"https://api.bitbucket.org/2.0/repositories/{repository_path}/refs/tags?sort=-name&pagelen=100")
        request.raise_for_status()
        tags_objects = request.json()['values']
        request.close()

        version_pattern = re.compile('^(\d+)\.(\d+)\.(\d+)$')
        tags = [tag['name'] for tag in tags_objects if version_pattern.fullmatch(tag['name'])]

        if not tags:
            return None

        def parse_version(version):
            return tuple(map(int, version.split('.')))

        return max(tags, key=parse_version)

    def get_repository_info(self, repository_path: str) -> dict:
        """
        Retrieves repository information
        :param repository_path: Repository path (ex: account/repo)
        :return: JSON response of repository information
        """
        request = requests.get(f"https://api.bitbucket.org/2.0/repositories/{repository_path}/")
        request.raise_for_status()
        request.close()
        return request.json()

    def readme_exists(self, repository_path: str, version: str) -> bool:
        """
        Determine if README.md file exists
        :param repository_path: Repository path (ex: account/repo)
        :param version: Reference (i.e. tag or commit)
        :return: True if exists, False otherwise
        """
        request = requests.head(f"https://bitbucket.org/{repository_path}/raw/{version}/README.md")
        request.raise_for_status()
        request.close()
        return request.status_code == requests.codes.ok

    def get_yml_definition(self, repository_path: str, version: str) -> Optional[str]:
        """
        Determine if README.md file exists
        :param repository_path: Repository path (ex: account/repo)
        :param version: Reference (i.e. tag or commit)
        :return: True if exists, False otherwise
        """
        request = requests.get(f"https://bitbucket.org/{repository_path}/raw/{version}/README.md")
        request.encoding = "utf-8"
        request.close()

        search_group = re.search(r"## YAML Definition(?:(?:.*?\n)*?)```yaml((?:.*?\n)*?)```$", request.text, re.MULTILINE)
        if search_group is None:
            return None
        yml_definition = (search_group.group(1)).lstrip()

        return yml_definition

    def get_pipe_metadata(self, repository_path: str, version: str) -> dict:
        """
        Retrieves pipe metadata
        :param repository_path: Repository path (ex: account/repo)
        :param version: Reference (i.e. tag or commit)
        :return: YAML object
        """
        request = requests.get(f"https://bitbucket.org/{repository_path}/raw/{version}/pipe.yml")
        request.raise_for_status()
        request.close()
        return yaml.safe_load(request.text)

    def get_pipe_created_timestamp(self, infile: str) -> str:
        process_exec = subprocess.Popen([f"git log --format=%aD {infile} | tail -1"], shell=True, stdout=subprocess.PIPE)
        return f"\"{process_exec.communicate()[0]}\""


class Check:

    @staticmethod
    def check_manifest_file(context: RuleContext) -> List[str]:
        failures = []
        if not LOCAL_METADATA.validate(context.local_yml):
            failures.append(f"File {context.infile} not valid: {str(LOCAL_METADATA.errors)}")
        return failures

    @staticmethod
    def check_git_tag(context: RuleContext) -> List[str]:
        bitbucket_api_service = BitbucketCloudService()
        yml = context.final_yml
        failures = []
        version = yml['version']
        repository_path = yml['repositoryPath']
        # Get latest tag
        latest_tag = bitbucket_api_service.get_latest_tag(repository_path)
        if latest_tag is None:
            failures.append(f"Repository does not contain any tag. Please, release a version first.")
        elif latest_tag != version:
            failures.append(f"{latest_tag} version available. Current version is {version}")
        return failures

    @staticmethod
    def check_readme(context: RuleContext) -> List[str]:
        bitbucket_api_service = BitbucketCloudService()
        yml = context.final_yml
        failures = []
        version = yml['version']
        repository_path = yml['repositoryPath']
        # Fetch the README.md
        if not bitbucket_api_service.readme_exists(repository_path, version):
            failures.append(f"{context.infile} repository not valid: README.md not found")
        return failures

    @staticmethod
    def check_yml_fragment(context: RuleContext) -> List[str]:
        bitbucket_api_service = BitbucketCloudService()
        yml = context.final_yml
        infile = context.infile
        failures = []
        version = yml['version']
        repository_path = yml['repositoryPath']
        yml_definition = bitbucket_api_service.get_yml_definition(repository_path, version)
        if 'yml' in yml and yml_definition != yml['yml']:
            # TODO: check this until we make sure all code snippets are similar
            failures.append(f"{infile} yml field not equal")
        if not yml['yml']:
            failures.append(f"{infile} yml definition in README.md does not exist")
        try:
            yaml.safe_load(yml['yml'])
        except yaml.YAMLError as exc:
            failures.append(f"{infile} yml definition in README.md is not valid")

        return failures

    @staticmethod
    def check_yml_definition_in_readme(context: RuleContext) -> List[str]:
        failures = []
        infile = context.infile
        yml = context.final_yml
        # validate YAML file
        try:
            yaml.safe_load(yml['yml'])
        except yaml.YAMLError as exc:
            failures.append(f"{infile} yml definition in README.md is not valid")
        return failures

    @staticmethod
    def check_docker_image_size(context: RuleContext) -> List[str]:
        failures = []
        if not context.remote_yml:
            return failures

        dockerhub_api_service = DockerHubAPIService()
        image_name = context.remote_yml['image']
        # Validate docker image size
        try:
            image_size = dockerhub_api_service.get_size(image_name)
        except:
            client = docker.from_env()
            image = client.images.pull(image_name)
            client.close()
            image_size = int(image.attrs['Size'])

        if image_size > 1073741824:
            failures.append(f"Docker image '{image_name}' is larger than 1GB.")
        return failures

    @staticmethod
    def check_remote_yml(context: RuleContext) -> List[str]:
        failures = []
        infile = context.infile
        if not REMOTE_METADATA.validate(context.remote_yml):
            failures.append(f"{infile} repository not valid: pipe.yml not valid: {str(REMOTE_METADATA.errors)}")
        return failures

    @staticmethod
    def check_final_yml(context: RuleContext) -> List[str]:
        failures = []
        infile = context.infile
        if not FINAL_METADATA.validate(context.final_yml):
            failures.append(f"{infile} final metadata not valid: {str(FINAL_METADATA.errors)}")
        return failures


def validate(context: RuleContext) -> tuple:
    failures = []
    skipped = []

    infile = context.infile
    # RuleEngine
    failures.extend(Check.check_manifest_file(context))
    failures.extend(Check.check_yml_fragment(context))
    failures.extend(Check.check_git_tag(context))
    failures.extend(Check.check_readme(context))
    failures.extend(Check.check_remote_yml(context))
    failures.extend(Check.check_docker_image_size(context))
    failures.extend(Check.check_final_yml(context))
    # Review skipped
    if infile in WHITELIST_PIPES:
        for f in failures:
            if f in WHITELIST_PIPES[infile]:
                skipped.append(f)

    failures = [item for item in failures if item not in skipped]
    return failures, skipped


def extract_information(local_yml: dict, infile: str) -> RuleContext:
    bitbucket_api_service = BitbucketCloudService()

    local_yml_valid = LOCAL_METADATA.validate(local_yml)
    if not local_yml_valid:
        warnings.warn("Structure you are using, is deprecated. Please, update to current one", DeprecationWarning)

    final_yml = local_yml.copy()
    remote_yml = None

    repository_path = final_yml['repositoryPath']
    version = final_yml['version']

    # Fetch Repository information
    if 'logo' not in final_yml:
        try:
            repo_info = bitbucket_api_service.get_repository_info(repository_path)
            final_yml['logo'] = repo_info['links']['avatar']['href']
        except HTTPError as error:
            logger.debug(error)
    if 'yml' not in final_yml:
        try:
            yml_definition = bitbucket_api_service.get_yml_definition(repository_path, version)
            final_yml['yml'] = yml_definition
        except HTTPError as error:
            logger.debug(error)
    # Fetch the pipe.yml (metadata)
    try:
        remote_yml = bitbucket_api_service.get_pipe_metadata(repository_path, version)
        remote_yml_valid = REMOTE_METADATA.validate(remote_yml)
        if not remote_yml_valid:
            warnings.warn("Structure you are using, is deprecated. Please, update to current one", DeprecationWarning)
    except HTTPError as error:
        logger.debug(error)

    # TODO: remove once custom pipes will change to new version
    if 'maintainer' in final_yml:
        remote_yml.pop('maintainer', None)
    if 'vendor' in final_yml:
        remote_yml.pop('vendor', None)

    final_yml = {key: value for key, value in {**final_yml, **remote_yml}.items() if key in FINAL_METADATA.schema}
    final_yml['timestamp'] = bitbucket_api_service.get_pipe_created_timestamp(infile)

    return RuleContext(infile=infile, local_yml=local_yml, remote_yml=remote_yml, final_yml=final_yml)


def process(infile: str) -> ProcessResult:
    failures = []
    errors = []
    skipped = []
    yaml_file = None
    with open(infile, 'r') as stream:
        try:
            manifest = yaml.safe_load(stream)
            context = extract_information(local_yml=manifest, infile=infile)
            yaml_file = context.final_yml
            failures, skipped = validate(context)
        except Exception as e:
            errors.append(str(e))

        print(f"Pipe: {infile}. Failures: " + str(len(failures)) + ", Errors: " + str(len(errors)) + ", Skipped: " + str(len(skipped)))
        return ProcessResult(
            metadata=ProcessMetadata(
                file=infile,
                valid=(len(failures) == 0 and len(errors) == 0),
                failures=failures,
                errors=errors,
                skipped=skipped),
            data=yaml_file)


def extract_data(results: List[ProcessResult]) -> List[dict]:
    data: List[dict] = [result.data for result in results if result.data]
    return data


def extract_errors(results: List[ProcessResult]) -> List[ProcessResult]:
    errors: List[ProcessResult] = [x for x in results if not x.metadata.valid]
    return errors


def main():
    path = sys.argv[1] if len(sys.argv) > 1 else 'pipes/*.yml'
    list_files = sorted(glob.glob(path))
    logger.info("Starting: " + str(len(list_files)) + " pipe(s) to verify.")

    t1 = datetime.now()

    with ThreadPoolExecutor() as executor:
        results = list(executor.map(process, list_files))

        errors = extract_errors(results)
        if errors:
            for error in errors:
                logger.error(f"{asdict(error.metadata)}")
            sys.exit(1)
        data = extract_data(results)

    with open('pipes.json', 'w', encoding='utf8') as outfile:
        json.dump(data, outfile, ensure_ascii=False)

    t2 = datetime.now()
    logger.info("Finished. Time: " + str(t2 - t1))


if __name__ == '__main__':
    main()
